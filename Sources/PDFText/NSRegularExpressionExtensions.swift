//
//  NSRegularExpressionExtensions.swift
//  
//
//  Created by Jeremy Pereira on 16/10/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

public extension NSRegularExpression
{

	/// Matches a Swift string in the Swift range passed in
	/// - Parameters:
	///   - in: The string to match
	///   - options: Matching options
	///   - range: Range in the string to check for matches
	/// - Returns: An array of match results
	func matches(in: String, options: MatchingOptions = [], range: Range<String.Index>) -> [NSTextCheckingResult]
	{
		let nsRange = NSRange(range, in: `in`)
		return self.matches(in: `in`, options: options, range: nsRange)
	}
}

public extension NSTextCheckingResult
{

	/// Get the piece of string forthe named capture group
	///
	/// - Parameters:
	///   - name: name of the named capture group in the regular expression
	///   - matchedString: The string in which the match occurred
	/// - Returns: Either the content of the capture group or `nil` if there was
	///            no match.
	func capture<S: StringProtocol>(withName name: String, in matchedString: S) -> String?
	{
		guard let range = Range(self.range(withName: name), in: matchedString)
		else { return nil }
		return String(matchedString[range])
	}

	/// Convert the range of this match to a Swift range
	///
	/// - Parameter string: The string to map this range to
	/// - Returns: A swift range or `nil` if our range is empty
	func range<S: StringProtocol>(in string: S) -> Range<String.Index>?
	{
		Range(self.range, in: string)
	}
}
