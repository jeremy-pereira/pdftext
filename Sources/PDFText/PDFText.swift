//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import PDFKit
import Toolbox

private let log = Logger.getLogger("PDFText.PDFText")

/// Represents a textual version of a wrapped PDF
public struct PDFText
{

	private var doc: PDFDocument


	/// Initialise a PDFText from a PDF at the given file path
	///
	/// - Parameter docPath: Path of PDF
	public init(_ docPath: String) throws
	{
		let url = URL(fileURLWithPath: docPath)
		try self.init(url: url)
    }

	/// Initialise a PDFText from a PDF at the given URL
	/// - Parameter url: URL referencing the PDF
	public init(url: URL) throws
	{
		guard let doc = PDFDocument(url: url) else { throw Error.cannotOpen(url) }
		self.doc = doc
	}

	/// The number of pages in the PDF
	public var pageCount: Int { doc.pageCount }
}

public extension PDFText
{

	/// A sequence that represents the PDF pages
	struct PageSequence: Sequence, IteratorProtocol
	{
		let doc: PDFDocument
		var nextIndex = 0

		init(doc: PDFDocument)
		{
			self.doc = doc
		}

		mutating public func next() -> PDFPageText?
		{
			guard nextIndex < doc.pageCount else { return nil }
			let ret = doc.page(at: nextIndex)
			nextIndex += 1
			return PDFPageText(page: ret!) // Assume pages are contiguous by index
		}
	}

	/// The pages in this PDF
	var pages: PageSequence { PageSequence(doc: doc) }
}

extension PDFText
{
	public enum Error: Swift.Error
	{
		case cannotOpen(URL)
	}
}
/// A textual representation of a PDF page
public struct PDFPageText
{
	private let page: PDFPage

	fileprivate init(page: PDFPage)
	{
		self.page = page
	}

	/// Construct the text of the page
	///
	/// PDFs don't really have the concept of text strings but each character is
	/// at a coordinate. If we sort them according to their coordinates, we
	/// should be able to reconstruct the text on the page.
	/// - Returns: The text of the page, hopefully in the right order
	public func constructText() -> String
	{
		guard let text = page.string else { return "" }
		guard page.numberOfCharacters == text.count
		else { fatalError("Expected string to have the same number of characters as page says there are") }

		var characters: [(Character, NSRect)] = text.enumerated().map
		{
			let (i, character) = $0
			return (character, page.characterBounds(at: i))
		}
		// Order the characters by their position on the page
		characters.sort
		{
			let firstRect = $0.1
			let secondRect = $1.1

			let firstYRounded = firstRect.origin.y.rounded()
			let secondYRounded = secondRect.origin.y.rounded()
			// Coordinates are maths convention (origin is at the bottom left)
			return firstYRounded > secondYRounded
			|| (firstYRounded == secondYRounded && firstRect.origin.x < secondRect.origin.x)
		}
		// Look for places where we have run two lines into one and where we
		// have split a single line
		for (i, (_, rect)) in characters.enumerated().dropFirst()
		{

			let (previousChar, previousRect) = characters[i - 1]
			let firstYRounded = previousRect.origin.y.rounded()
			let secondYRounded = rect.origin.y.rounded()
			if previousChar != "\n" && previousRect.origin.x > rect.origin.x
			{
				precondition(previousChar == " ", "Assumption that there would be a space at \(i) is incorrect")
				// The previous character is to the right of this one
				characters[i - 1] = ("\n", previousRect)
			}
			else if previousChar == "\n" && firstYRounded == secondYRounded
			{
				characters[i - 1] = (" ", previousRect)
			}
		}
		log.debug("\(characters)")
		return String(characters.map { $0.0 })
	}
}
