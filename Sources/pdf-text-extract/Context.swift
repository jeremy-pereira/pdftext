//
//  Context.swift
//  
//
//  Created by Jeremy Pereira on 17/10/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Statistics

struct Context
{
	var pdfPageIndex: Int = 0
	var pageNumber: Int? = nil
	var printDate: String? = nil
	var payDate: PayDate?

	let pages: Statistics<Page>
	let payDates: Statistics<PayDate>
	let lineItems: Statistics<LineItem>

	init(output: String) throws
	{
		pages = try Statistics<Page>(sqlite3File: output)
		payDates = try Statistics<PayDate>(sqlite3File: output)
		lineItems = try Statistics<LineItem>(sqlite3File: output)
		try pages.clear()
		try payDates.clear()
		try lineItems.clear()
	}

	mutating func start(payDate: String) throws
	{
		// We had a pay date butr we missed the total, record it anyway as long
		// as it's not just a continuation
		if var oldPayDate = self.payDate
		{
			guard oldPayDate.payDate != payDate else { return }
			oldPayDate.endPage = pdfPageIndex
			try payDates.record(dataPoint: oldPayDate)
		}
		self.payDate = PayDate(startPage: pdfPageIndex, payDate: payDate)
	}

	mutating func end(payDate: String, lineItems: Int) throws
	{
		guard var existingPayDate = self.payDate
		else { throw Error.missedStart(payDate: payDate) }
		guard existingPayDate.payDate == payDate
		else { throw Error.payDateMismatch(existing: existingPayDate.payDate, new: payDate) }

		existingPayDate.lineItems = lineItems
		existingPayDate.endPage = pdfPageIndex
		try payDates.record(dataPoint: existingPayDate)
		self.payDate = nil
	}

	func recordPage() throws
	{
		try pages.record(dataPoint: Page(index: pdfPageIndex, pageNumber: pageNumber, printDate: printDate))
	}

	func recordLineItem(polRef: String, tranType: String, dateRaised: String, origDebt: String, poac: String, commAmount: String, name: String) throws
	{
		let lineItem = try LineItem(pdfPageIndex: pdfPageIndex,
								    payDate: payDate?.payDate,
								    polRef: polRef,
								    tranType: tranType,
								    dateRaised: dateRaised,
								    origDebtString: origDebt,
								    poacString: poac,
								    commAmountString: commAmount,
									name: name)
		try lineItems.record(dataPoint: lineItem)
	}
}

extension Context
{
	struct Page: Recordable
	{
		let data: [String : StatsMappable]

		init(index: Int, pageNumber: Int?, printDate: String?)
		{
			self.data = [
				"pdfIndex" : index,
				"pageNumber" : pageNumber,
				"printDate" : printDate
			]
		}
	}

	struct PayDate: Recordable
	{
		let startPage: Int
		var endPage: Int?
		let payDate: String
		var lineItems: Int?

		init(startPage: Int, endPage: Int? = nil, payDate: String, lineItems: Int? = nil)
		{
			self.startPage = startPage
			self.endPage = endPage
			self.payDate = payDate
			self.lineItems = lineItems
		}

		var data: [String : StatsMappable]
		{
			return [
				"startPdfPage" : startPage,
				"endPdfPage" : endPage,
				"payDate" : payDate,
				"lineItems" : lineItems
			]
		}
	}

	struct LineItem: Recordable
	{
		let data: [String : StatsMappable]

		init(pdfPageIndex: Int, payDate: String?, polRef: String, tranType: String, dateRaised: String, origDebtString: String, poacString: String, commAmountString: String, name: String) throws
		{
			guard let origDebt = Double(origDebtString)
			else { throw Error.invalidCurrency(origDebtString) }
			guard let poac = Double(poacString)
			else { throw Error.invalidCurrency(poacString) }
			guard let commAmount = Double(commAmountString)
			else { throw Error.invalidCurrency(commAmountString) }
			data = [
				"pdfPage" : pdfPageIndex,
				"payDate" : payDate,
				"polRef" : polRef,
				"tranType" : tranType,
				"dateRaised" : dateRaised,
				"origDebt" : origDebt,
				"poac" : poac,
				"commAmout" : commAmount,
				"name" : name
			]
		}
	}
}

extension Context
{
	enum Error: Swift.Error
	{
		case payDateMismatch(existing: String, new: String)
		case missedStart(payDate: String)
		case invalidCurrency(String)
	}
}
