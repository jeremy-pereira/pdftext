//
//  main.swift
//  
//
//  Created by Jeremy Pereira on 15/10/2021.
//
//  Copyright (c) Jeremy Pereira xxxx
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import ArgumentParser
import PDFText
import Toolbox

private let log = Logger.getLogger("pdf-text-extract.main")

struct PDFTextParser: ParsableCommand
{
	@Option(help: "SQLite3 database in which to put the output")
	var outputDatabase: String
	@Option(help: "Turn on debug for a specific page index (0 based)")
	var debugPage: Int?
	@Flag(help: "Output debug information")
	var debug: Bool = false
	@Argument(help: "The PDF file from which to extract the text")
	var fileName: String


	func run() throws
	{
		log.level = debug ? .debug: .info
		let doc = try PDFText(fileName)
		log.info("Pages: \(doc.pageCount)")

		let datePattern = #"\d+/\d+/\d+"#
		let currencyPattern = #"-?\d+\.\d{2}"#
		let polRef = #"(?<polRef>[A-Z]{4}\d{2}([A-Z]{2}| {1,2})\d{2})"#
		let tranType = #"(?<tranType>Adjustment|Renewal|Charge|New Business|Cancellation|Endorsement|Journal|Transfrd NB)"#
		let dateRaised = #"(?<dateRaised>\#(datePattern))"#
		let origDebt = #"(?<origDebt>\#(currencyPattern))"#
		let poac = #"(?<poac>\#(currencyPattern))"#
		let commAmount = #"(?<commAmount>\#(currencyPattern))"#

		let headerRE = try NSRegularExpression(pattern: #"(?<printDate>\#(datePattern)-\d+:\d+)\s*Transactions\s*Page\s*(?<pageNumber>\d+)"#,
											   options: [.dotMatchesLineSeparators])
		let payDateRE = try NSRegularExpression(pattern: #"pay.dt (?<payDate>\*\* None \*\*|\#(datePattern))\s+(?<continued>\(cont\.\)|)"#,
												options: [.dotMatchesLineSeparators])
		let lineItemRE = try NSRegularExpression(pattern: #"^\s*\#(polRef)\s\#(tranType)\s\#(dateRaised)\s\#(origDebt)\s\#(poac)\s\#(commAmount)\s*(?<name>[^\n]*)\n"#,
												 options: [.dotMatchesLineSeparators])

		let totalRE = try NSRegularExpression(pattern: #"Total\sfor\spay.dt\s(?<payDate>\*\* None \*\*|\#(datePattern))\s*\((?<lineItemCount>\d+)\)"#,
											  options: [.dotMatchesLineSeparators])
		var context = try Context(output: outputDatabase)

		for (i, page) in doc.pages.enumerated()
		{
			// This is for testing during dev
			if let debugPage = debugPage, i == debugPage
			{
				log.pushLevel(.debug)
				//Logger.pushLevel(.debug, forName: "PDFText.PDFText")
			}
			defer
			{
				if let debugPage = debugPage, i == debugPage
				{
					log.popLevel()
					//Logger.popLevel(forName: "PDFText.PDFText")
				}
			}

			context.pdfPageIndex = i
			var pageText = page.constructText()
			log.debug("Index \(i) ========================\n\(pageText)\n================")
			guard try match(regularExpression: headerRE, pageText: &pageText, context: &context, process: parseHeader)
			else { throw Error.noHeaderFound(index: i) }
			while try match(regularExpression: payDateRE, pageText: &pageText, context: &context, process: parsePayDate)
			{
				log.debug("Text after payDate: =====\n\(pageText.prefix(100))\n=====")
				log.debug("Pay date found: \(context.payDate?.payDate ?? "<<nil>>")")
				while try match(regularExpression: lineItemRE, pageText: &pageText, context: &context, process: parseLineItem)
				{
					log.debug("Text after line item: =\n\(pageText.prefix(100))\n=")
				}
				_ = try match(regularExpression: totalRE, pageText: &pageText, context: &context, process: parseEndPayDate)
			}
			log.debug("Remaining text on page \(i) \n========================\n\(pageText)\n================")
			try context.recordPage()
		}
	}

	private func match(regularExpression: NSRegularExpression,
					   pageText: inout String,
					   context: inout Context,
					   process: (NSTextCheckingResult, String, inout Context) throws -> ()) rethrows -> Bool
	{
		let matches = regularExpression.matches(in: pageText, range: pageText.startIndex ..< pageText.endIndex)
		guard !matches.isEmpty else { return false }
		let match = matches.first!
		defer { consumeMatch(match: match, text: &pageText) }

		try process(match, pageText, &context)
		return true
	}

	private func parseHeader(match: NSTextCheckingResult, pageText: String, context: inout Context) throws
	{
		if let printDate = match.capture(withName: "printDate", in: pageText)
		{
			if let existingPrintDate = context.printDate
			{
				if existingPrintDate != printDate
				{
					log.warn("Page at index \(context.pdfPageIndex) has non matching print date '\(printDate)', expected '\(existingPrintDate)'")
				}
			}
			else
			{
				context.printDate = printDate
			}
		}
		else
		{
			log.warn("No print date in header")
		}
		if let pageNumber = match.capture(withName: "pageNumber", in: pageText)
		{
			guard let numberAsInt = Int(pageNumber) else { throw Error.nonNumericPage(index: context.pdfPageIndex, pageNumber: pageNumber) }
			context.pageNumber = numberAsInt
		}
		else
		{
			log.warn("No page number in header")
		}
	}

	private func parsePayDate(match: NSTextCheckingResult, pageText: String, context: inout Context) throws
	{
		let ret = try getCapture(match: match, name: "payDate", text: pageText)
		try context.start(payDate: ret)
	}

	private func parseLineItem(match: NSTextCheckingResult, pageText: String, context: inout Context) throws
	{
		let polRef = try getCapture(match: match, name: "polRef", text: pageText)
		let tranType = try getCapture(match: match, name: "tranType", text: pageText)
		let dateRaised = try getCapture(match: match, name: "dateRaised", text: pageText)
		let origDebt = try getCapture(match: match, name: "origDebt", text: pageText)
		let poac = try getCapture(match: match, name: "poac", text: pageText)
		let commAmount = try getCapture(match: match, name: "commAmount", text: pageText)
		let name = try getCapture(match: match, name: "name", text: pageText)
		log.debug("Line item found: \(polRef), \(tranType), \(name)")
		try context.recordLineItem(polRef: polRef, tranType: tranType, dateRaised: dateRaised, origDebt: origDebt, poac: poac, commAmount: commAmount, name: name)
	}

	private func parseEndPayDate(match: NSTextCheckingResult, pageText: String, context: inout Context) throws
	{
		guard let payDate = match.capture(withName: "payDate", in: pageText)
		else { throw Error.missing(capture: "payDate") }
		guard let lineItemString = match.capture(withName: "lineItemCount", in: pageText)
		else { throw Error.missing(capture: "lineItemCount") }
		guard let lineItemCount = Int(lineItemString)
		else { throw Error.nonNumericCount }
		log.debug("End pay date: \(payDate), line items \(lineItemCount)")
		try context.end(payDate: payDate, lineItems: lineItemCount)
	}

	private func consumeMatch(match: NSTextCheckingResult, text: inout String)
	{
		guard let range = match.range(in: text)
		else { fatalError("Can't get an empty range with match") }
		let residueRange = range.upperBound ..< text.endIndex
		text = String(text[residueRange])

	}

	private func getCapture(match: NSTextCheckingResult, name: String, text: String) throws -> String
	{
		guard let ret = match.capture(withName: name, in: text) else
		{
			throw Error.missing(capture: name)
		}
		return ret
	}
}

extension PDFTextParser
{
	/// Errors that may be thrown by the program
	enum Error: Swift.Error
	{
		/// A page contains multiple matches for its header pattern
		///
		/// This would indicate a false assumption that the header is really a header
		/// - Parameter index: The PDF page index
		case multipleHeaders(index: Int)
		/// A page contains nothing that will match  the header
		///
		/// - Parameter index: The PDF page index
		case noHeaderFound(index: Int)
		/// Page number extracted isn't a number
		///
		/// - Parameters:
		///   - index: PDF page index
		///   - pageNumber: The page number that can't be parsed as a number
		case nonNumericPage(index: Int, pageNumber: String)
		case missing(capture: String)
		case nonNumericCount
	}
}

PDFTextParser.main()
