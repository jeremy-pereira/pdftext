import XCTest
import PDFText
import PDFKit

final class PDFTextTests: XCTestCase
{
	var doc: PDFText!

	override func setUpWithError() throws
	{
		try super.setUpWithError()
		guard let testUrl = Bundle.module.url(forResource: "test", withExtension: "pdf")
		else
		{
			throw Error.noTestFile
		}
		doc = try PDFText(url: testUrl)
	}

    func testPageCount() throws
	{
		XCTAssert(doc.pageCount == 1)
    }

	func testText()
	{
		var count = 0
		for page in doc.pages
		{
			count += 1
			let text = page.constructText()
			XCTAssert(text.contains("PDFTextTests"))
		}
		XCTAssert(count == 1)
	}

}

extension XCTestCase
{
	enum Error: Swift.Error
	{
		case noTestFile
	}
}
