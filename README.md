# PDFText

A wrapper for [PDFKit](https://developer.apple.com/documentation/pdfkit) that makes it a bit easier to extract text from a PDF.

This is a Swift package.


