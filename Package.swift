// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PDFText",
	platforms: [.macOS(.v11)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "PDFText",
            targets: ["PDFText"]),
    ],
    dependencies: [
		.package(url: "https://github.com/apple/swift-argument-parser", from: "1.0.1"),
		.package(url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/statistics.git", from: "4.0.1"),
		.package(name: "Toolbox", url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/toolbox.git", from: "21.0.0"),
   ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "PDFText",
            dependencies: ["Toolbox"]),
        .testTarget(
            name: "PDFTextTests",
            dependencies: ["PDFText", "Toolbox"],
			resources: [.copy("test.pdf")]),
		.executableTarget(
			name: "pdf-text-extract",
			dependencies: ["PDFText",
						   "Toolbox",
						   .product(name: "Statistics", package: "statistics"),
						   .product(name: "ArgumentParser", package: "swift-argument-parser")],
			linkerSettings: [.linkedFramework("PDFKit")]),

    ]
)
